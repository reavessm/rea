build:
	go build -o rea cmd/rea/main.go

.PHONY: build

clean:
	rm -f rea out.go

.PHONY: clean
