# REA

`rea` is a **R**everse-**E**ngineering **A**pi generator

## Usage

```
> rea -h
Usage of rea:
  -f string
        File to read from
  -o string
        File to output to (default "out.go")
  -p string
        Package name of generated file (default "main")
  -s string
        Name of struct to be generated (default "Object")
  -v    Verbose output?
```

## Examples

### Basic 

```sh
curl 'https://www.boredapi.com/api/activity' | rea && cat out.go
```

### Advanced

```sh
rea -w 'https://randomuser.me/api/' -s User -p accounts -o user.go && cat user.go
```

### Appending

```sh
rea -w https://www.boredapi.com/api/activity -o foo.go -s activity
ocm whoami | rea -o foo.go -s user
cat foo.go
```

## Installation

```sh
go install gitlab.com/reavessm/rea/cmd/rea@latest
```
