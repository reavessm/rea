/**
 * File: main.go
 * Written by: Stephen M. Reaves
 * Created on: Tue, 29 Mar 2022
 */

package main

import (
	"encoding/json"
	"fmt"
	"go/format"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"gitlab.com/reavessm/rea/pkg/config"
	"gitlab.com/reavessm/rea/pkg/generators"
)

func main() {
	if err := config.ParseFlags(); err != nil {
		log.Fatalln("Error! ", err)
	}

	if err := run(); err != nil {
		log.Fatalln("Error! ", err)
	}
}

func run() error {
	var b []byte
	var e error

	if config.FromStdIn {
		b, e = ioutil.ReadAll(os.Stdin)
	} else if config.WebsiteName != "" {
		b, e = get(config.WebsiteName)
	} else {
		b, e = os.ReadFile(config.Input)
	}
	if e != nil {
		return e
	}

	if config.Verbose {
		fmt.Println("Input:")
		fmt.Println(string(b))
	}

	if err := parseAndWrite(b); err != nil {
		return err
	}

	if err := formatOutput(config.Output); err != nil {
		return err
	}

	if config.Verbose {
		fmt.Println("Output written to: ", config.Output)
	}

	return nil
}

func get(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return []byte(""), err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}

func formatOutput(filename string) error {
	var file *os.File
	var err error

	file, err = os.Open(filename)
	if err != nil {
		return err
	}

	stat, err := file.Stat()
	if err != nil {
		return err
	}

	out, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	out, err = format.Source(out)
	if err != nil {
		return err
	}

	return os.WriteFile(filename, out, stat.Mode())
}

func parseAndWrite(j []byte) error {
	var b map[string]interface{}
	if err := json.Unmarshal(j, &b); err != nil {
		return err
	}

	data := generators.TplTuple{
		Name:   config.StructName,
		Fields: b,
	}

	var out *os.File
	var err error

	if config.Append {
		out, err = os.OpenFile(config.Output, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
	} else {
		out, err = os.Create(config.Output)
	}
	if err != nil {
		return err
	}
	defer out.Close()

	t := generators.New()
	t.NestedParse("", data)

	_, err = t.Parse()
	if err != nil {
		return err
	}

	err = t.Execute(out, data)
	if err != nil {
		return err
	}

	return nil
}
