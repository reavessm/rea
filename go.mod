module gitlab.com/reavessm/rea

go 1.16

require (
	github.com/docker/distribution v2.8.2+incompatible
	github.com/drhodes/golorem v0.0.0-20220328165741-da82e5b29246
)
