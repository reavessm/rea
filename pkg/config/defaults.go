package config

const (
	DefaultOutput     string = "out.go"
	DefaultPkgName    string = "main"
	DefaultStructName string = "Object"
)

var (
	Input       string = ""
	Output      string = DefaultOutput
	PkgName     string = DefaultPkgName
	StructName  string = DefaultStructName
	WebsiteName string = ""
	Verbose     bool   = false
	FromStdIn   bool   = false
	Append      bool   = false
)
