package config

import (
	"errors"
	"flag"
	"os"
)

func ParseFlags() error {
	stat, _ := os.Stdin.Stat()
	if (stat.Mode() & os.ModeCharDevice) == 0 {
		FromStdIn = true
	}

	if len(os.Args) <= 1 && !FromStdIn {
		return errors.New("Either pipe json from stdin or provide filename with -f flag")
	}

	fileName := flag.String("f", "", "File to read from")
	output := flag.String("o", DefaultOutput, "File to output to")
	pkgName := flag.String("p", DefaultPkgName, "Package name of generated file")
	verbose := flag.Bool("v", false, "Verbose output?")
	structName := flag.String("s", DefaultStructName, "Name of struct to be generated")
	websiteName := flag.String("w", "", "Provide a url instead of a filename to have rea `curl` it for your (experimental)")
	shouldAppend := flag.Bool("a", false, "Append?")

	flag.Parse()

	if *fileName != "" {
		Input = *fileName
	} else if !FromStdIn && *websiteName == "" {
		return errors.New("Either pipe json from stdin or provide filename with -f flag")
	}

	if *output != "" {
		Output = *output
	}

	if *pkgName != "" {
		PkgName = *pkgName
	}

	if *verbose {
		Verbose = true
	}

	if *structName != "" {
		StructName = *structName
	}

	if *websiteName != "" {
		WebsiteName = *websiteName
	}

	if *shouldAppend {
		Append = true
	}

	return nil
}
